#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_manifest_files "$STEAM_APP_ID_LIST"

# build instructions
#
pushd source/neo || exit 1
mkdir build
cd build || exit 1
cmake -DCMAKE_INSTALL_PREFIX=../../../tmp ..
make -j "$(nproc)"
make install
popd || exit 1

find tmp/

cp -rfv tmp/bin/* "9050/dist/"
cp -rfv tmp/lib/dhewm3/* "9050/dist/"

find "9050"

cp -rfv tmp/bin/* "9070/dist/"
cp -rfv tmp/lib/dhewm3/* "9070/dist/"

find "9070"
